#! /bin/bash

# Need Root priveleges to accomplish install
if [ $EUID != 0 ]; then
    sudo "$0" "$@"
    exit $?
fi

# Copy the files to the correct location
cp ./StandAlone/drivedetect.sh /usr/local/bin/
cp ./StandAlone/initialcopy.sh /usr/local/bin/

# Set permissions on the files to execute
chmod +x /usr/local/bin/drivedetect.sh
chmod +x /usr/local/bin/initialcopy.sh
   
if [ ! -f /etc/rc.d/rc.local ]; then
   echo "File rc.local does not exist at /etc/rc.d/"
   echo "You'll need to start the script /usr/local/bin/drivedetect.sh"
   echo "manually.  Sorry your Linux system sucks!"
   exit 1
else
   # Set rc.local to executable
   chmod +x /etc/rc.d/rc.local
   # Adding the line that will run the script at startup
   echo "/usr/local/bin/drivedetect.sh" >> /etc/rc.d/rc.local
fi

# End of the script
echo "Scripts installed successfully, Reboot your machine to start scripts!"
exit 0
