# Memory Card Offloader
This is a set of scripts that can be run as a standalone memory card offloader for pictures.

The main scripts are found in StandAlone, the installer script is located at the repo root.  You will need to chmod +x install.sh before
running it in order to execute the script.

Both detectdrive and initialcopy need to be copied to /usr/local/bin and set with execute permissions.  Once installed create a startup
job either in cron or in your startup administration panel (ubuntu) and set initialcopy to run at boot.  This should run the script as root
which will then spawn the detectdrive process that looks for the insertion of media cards that udev automounts in /media as a mass storage
device.  Once a drive is detected the detectdrive script is killed to flush the memory of it and any resources it may have been using.  The
initialcopy script then loops through all the directories available and copies the pictures it finds into a folder created within the 
~/Pictures/TempStore folder.  These files are renamed with the current date/time stamp in order to keep duplicates from overwriting each other
in the event that 2 camera cards are inserted that name their files the same thing. 
